import java.util.Scanner;

public class Vtoraya {

	public static void main(String[] args) {
		System.out.println("Vvedite kolvo zarabativaemih deneg v odin chas");
		Scanner scan = new Scanner(System.in);
		int zarplata = scan.nextInt();
		System.out.println("Vvedite kolvo prorabotannih chasov v nedelu");
		int chasi = scan.nextInt();
		int resultat = summa(zarplata, chasi);
		System.out.println("Zarplata v nedelu = "+resultat);
	}
	
	public static int summa(int zarplata, int chasi) {
		int resultat = 0;
		if (chasi < 168) {
		resultat = chasi * zarplata;
		}
		return resultat;
	}
}