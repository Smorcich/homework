package net.codejava;

import java.util.Scanner;

public class Tretya{

	public static void main(String[] args) {
		System.out.println("Vedite zarplatu v chas, a potom kol-vo chasov");
		Scanner scan = new Scanner(System.in);
		System.out.print("$");
		int zarplata = scan.nextInt();
		if (zarplata > 8) {
			int chasi = scan.nextInt();
			if (chasi <= 60) {
				if (chasi > 40) {
					double newChasi;
					newChasi = 40 + (chasi - 40) * 1.5;
					double resultat = zarplata * newChasi;
					System.out.println(resultat);
				} else {
					int resultat = zarplata * chasi;
					System.out.println(resultat);
				}
			} else
				System.out.println("Rabotnik ne mozhet rabotat' bolshe 60 chasov v nedeliu");
		} else
			System.out.println("Rabotnik ne mozhet poluchat' men'she 8$ v chas ");
	}
}